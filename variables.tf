variable "project" {
  type        = string
  description = "The project ID in which to manage the Memorystore resources"
}

variable "region" {
  type        = string
  description = "The region of the Memorystore resources"
  default     = "us-central1"
}

variable "name" {
  type        = string
  description = "Redis instance name"
}

variable "memory_size" {
  type        = number
  description = "Redis instance memory in GB"
  default     = 1
}

variable "authorized_network" {
  type        = string
  description = "GCP network to which Redis instance is connecting to"
}

variable "connect_mode" {
  type        = string
  description = "Connection mode of the Redis instance"
  default     = "PRIVATE_SERVICE_ACCESS"
}

variable "replica_count" {
  type        = number
  description = "Number of Redis replica instances, only relevant if tier is set to STANDARD_HA"
  default     = 1
}

variable "tier" {
  type        = string
  description = "Redis instance tier"
  default     = "BASIC"
}

variable "redis_version" {
  type        = string
  description = "Redis version"
  default     = "REDIS_6_X"
}

variable "maintenance_window_day" {
  type        = string
  description = "The day of week that maintenance updates occur"
  default     = "SATURDAY"
}

variable "maintenance_window_hours" {
  type        = number
  description = "Hours of day in 24 hour format. Should be from 0 to 23"
  default     = 12
}

variable "maintenance_window_minutes" {
  type        = number
  description = "Minutes of hour of day. Must be from 0 to 59"
  default     = 0
}

variable "persistence_mode" {
  type        = string
  description = "Controls whether Persistence features are enabled. Possible values are DISABLED and RDB"
  default     = "DISABLED"
}

variable "rdb_snapshot_period" {
  type        = string
  description = "Available snapshot periods for scheduling. Possible values are ONE_HOUR, SIX_HOURS, TWELVE_HOURS, and TWENTY_FOUR_HOURS"
  default     = ""
}

variable "redis_configs" {
  type        = map(string)
  description = "Optional Redis configuration parameters, e.g. maxmemory-policy."
  default     = {}
}
