GitLab.com Memorystore Redis Terraform Module

## What is this?

This module provisions a Google Memorystore Redis instance.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Usage

```terraform
module "customers-redis" {
  source  = "ops.gitlab.net/gitlab-com/memorystore-redis/google"
  version = "X.Y.Z"

  name               = "redis"
  project            = var.project
  region             = var.region
  authorized_network = "network-link"
  memory_size        = 1
  redis_version      = "REDIS_5_0"
  tier               = "BASIC"
}
```

Once you have loaded this module, you can reference the outputs to access the instance.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_redis_instance.instance](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/redis_instance) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_authorized_network"></a> [authorized\_network](#input\_authorized\_network) | GCP network to which Redis instance is connecting to | `string` | n/a | yes |
| <a name="input_connect_mode"></a> [connect\_mode](#input\_connect\_mode) | Connection mode of the Redis instance | `string` | `"PRIVATE_SERVICE_ACCESS"` | no |
| <a name="input_maintenance_window_day"></a> [maintenance\_window\_day](#input\_maintenance\_window\_day) | The day of week that maintenance updates occur | `string` | `"SATURDAY"` | no |
| <a name="input_maintenance_window_hours"></a> [maintenance\_window\_hours](#input\_maintenance\_window\_hours) | Hours of day in 24 hour format. Should be from 0 to 23 | `number` | `12` | no |
| <a name="input_maintenance_window_minutes"></a> [maintenance\_window\_minutes](#input\_maintenance\_window\_minutes) | Minutes of hour of day. Must be from 0 to 59 | `number` | `0` | no |
| <a name="input_memory_size"></a> [memory\_size](#input\_memory\_size) | Redis instance memory in GB | `number` | `1` | no |
| <a name="input_name"></a> [name](#input\_name) | Redis instance name | `string` | n/a | yes |
| <a name="input_persistence_mode"></a> [persistence\_mode](#input\_persistence\_mode) | Controls whether Persistence features are enabled. Possible values are DISABLED and RDB | `string` | `"DISABLED"` | no |
| <a name="input_project"></a> [project](#input\_project) | The project ID in which to manage the Memorystore resources | `string` | n/a | yes |
| <a name="input_rdb_snapshot_period"></a> [rdb\_snapshot\_period](#input\_rdb\_snapshot\_period) | Available snapshot periods for scheduling. Possible values are ONE\_HOUR, SIX\_HOURS, TWELVE\_HOURS, and TWENTY\_FOUR\_HOURS | `string` | `""` | no |
| <a name="input_redis_configs"></a> [redis\_configs](#input\_redis\_configs) | Optional Redis configuration parameters, e.g. maxmemory-policy. | `map(string)` | `{}` | no |
| <a name="input_redis_version"></a> [redis\_version](#input\_redis\_version) | Redis version | `string` | `"REDIS_6_X"` | no |
| <a name="input_region"></a> [region](#input\_region) | The region of the Memorystore resources | `string` | `"us-central1"` | no |
| <a name="input_replica_count"></a> [replica\_count](#input\_replica\_count) | Number of Redis replica instances, only relevant if tier is set to STANDARD\_HA | `number` | `1` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | Redis instance tier | `string` | `"BASIC"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_private_ip"></a> [private\_ip](#output\_private\_ip) | Private IP address of the Redis instance |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
