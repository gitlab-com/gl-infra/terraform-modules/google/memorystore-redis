output "private_ip" {
  value       = google_redis_instance.instance[*].host
  description = "Private IP address of the Redis instance"
}
