resource "google_redis_instance" "instance" {
  name               = var.name
  project            = var.project
  region             = var.region
  authorized_network = var.authorized_network
  connect_mode       = var.connect_mode
  memory_size_gb     = var.memory_size
  redis_version      = var.redis_version
  redis_configs      = var.redis_configs
  replica_count      = var.tier == "BASIC" ? 0 : var.replica_count
  tier               = var.tier

  maintenance_policy {
    weekly_maintenance_window {
      day = var.maintenance_window_day

      start_time {
        hours   = var.maintenance_window_hours
        minutes = var.maintenance_window_minutes
      }
    }
  }

  persistence_config {
    persistence_mode    = var.persistence_mode
    rdb_snapshot_period = var.rdb_snapshot_period
  }

  lifecycle {
    ignore_changes = [
      maintenance_schedule
    ]
  }
}
